#pragma once
#include "FunctionsAVX.h"
#include "Helpers.h"
#include "Constants.h"
#include "immintrin.h"

class FunctionsAVX
{
public:
	FunctionsAVX();
	static int SAD(volatile unsigned char* block_1, volatile unsigned char* block_2, int puWidth, int puHeight);
	static void quantization(volatile int16_t* A, int QP, int NB, int N, bool* is_zero_matrix, int16_t* C);
	static void dequantization(volatile int16_t* A, int QP, int bitDepth, int N, int16_t* C);
	static void multiply(volatile int16_t* A, volatile int16_t* B, int N, int16_t* C, int bdShift);
	static void dct(volatile int16_t* A, volatile int16_t* B, int N, int16_t* C);
	static void substract(volatile unsigned char* rawBlock, volatile  unsigned char* predictedBlock, int16_t* residualBlock, int blockSize);
	static void rgb2yuv(volatile int16_t* R, volatile int16_t* G, volatile int16_t* B, int blockSize, int16_t* Y, int16_t* U, int16_t* V);
	static void rgb2yuvShift(volatile int16_t* R, volatile int16_t* G, volatile int16_t* B, int blockSize, int16_t* Y, int16_t* U, int16_t* V);
};

