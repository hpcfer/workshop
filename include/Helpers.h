#pragma once
#include <iomanip>
#include <iostream>

class Helpers
{
public:
	//static void generateRandomResidual(unsigned char* mat, int blockSize);
	//static void generateRandomResidual(int16_t* mat, int blockSize);
	template <typename T>
	static void generateRandomResidual(T* mat, int blockSize, int scope)
	{
		for (int m = 0; m < blockSize * blockSize; m++)
		{
			mat[m] = (rand() % (2 * scope)) - scope;
		}
	}
	template <typename T>
	static void generateRandomPositiveResidual(T* mat, int blockSize, int scope)
	{
		for (int m = 0; m < blockSize * blockSize; m++)
		{
			mat[m] = rand() % scope;
		}
	}
	template <typename T>
	static void printMatrix(T* mat, int size)
	{
		for (int m = 0; m < size; m++)
		{
			for (int n = 0; n < size; n++)
			{
				std::cout << (int)mat[m * size + n] << " ";
			}
			std::cout << std::endl;
		}
	}
	template <typename T>
	static bool compareMatrices(T* a, T* b, int blockSize)
	{
		bool same = true;

		for (int i = 0; i < blockSize * blockSize; i++)
		{
			if (a[i] != b[i])
			{
				same = false;
				break;
			}
		}

		return same;
	}

	static int clip3(const int a, const int b, int x);
	static int sign(const int a);
	static int logarithm2(int a);
};

