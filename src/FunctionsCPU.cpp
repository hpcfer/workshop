﻿#include "FunctionsCPU.h"

FunctionsCPU::FunctionsCPU()
{
}

int FunctionsCPU::SAD(volatile unsigned char* block_1, volatile unsigned char* block_2, int puWidth, int puHeight)
{

	int result = 0;

	for (int i = 0; i < puHeight; ++i)
	{
		for (int j = 0; j < puWidth; ++j)
		{
			result += abs(block_1[i * puWidth + j] - block_2[i * puWidth + j]);
		}
	}

	return result;
}

void FunctionsCPU::quantization(volatile int16_t* A, int QP, int NB, int N, bool* is_zero_matrix, int16_t* C)
{
	int M = Helpers::logarithm2(N);
	int bdShift = 29 - M - NB;

	int coeffMin = -32768;
	int coeffMax = 32767;

	int mScalingFactor = 16;
	int i;
	*is_zero_matrix = true;

	for (i = 0; i < N; i++)
	{
		for (int j = 0; j < N; j++)
		{
			int sign = 1;
			if (*(A + i * N + j) < 0)
				sign = -1;

			*(C + i * N + j) = (((abs(*(A + i * N + j)) * f[QP % 6] + (1 << (bdShift - 1))) >> QP / 6) >> bdShift);
			*(C + i * N + j) = Helpers::clip3(coeffMin, coeffMax, *(C + i * N + j) * sign);

			if (*(C + i * N + j) != 0)
				*is_zero_matrix = false;
		}
	}



}

void FunctionsCPU::dequantization(volatile int16_t* A, int QP, int bitDepth, int N, int16_t* C)
{
	int log2TransformRange = 15;
	int bdShift = bitDepth + Helpers::logarithm2(N) + 10 - log2TransformRange;
	int coeffMin = -32768;
	int coeffMax = 32767;
	int mScalingFactor = 16;
	int i;

	for (i = 0; i < N; i++)
	{
		for (int j = 0; j < N; j++)
		{
			*(C + i * N + j) = Helpers::clip3(coeffMin, coeffMax, ((*(A + i * N + j) * mScalingFactor * g[QP % 6] << (QP / 6)) + (1 << (bdShift - 1))) >> bdShift);
		}
	}

}

void FunctionsCPU::multiply(volatile int16_t* A, volatile int16_t* B, int N, int16_t* C, int bdShift)
{
	for (int r = 0; r < N; r++) {
		for (int c = 0; c < N; c++) {
			int tmp = 0;
			for (int p = 0; p < N; p++) {
				tmp = tmp + A[r * N + p] * B[p * N + c];
			}
			C[r * N + c] = (tmp + (1 << (bdShift - 1))) >> bdShift;
		}
	}
}

void FunctionsCPU::dct(volatile int16_t* A, volatile int16_t* B, int N, int16_t* C)
{
	int log2TransformRange = 15;
	int bitDepth = 8;

	int coeffMin = -32768;
	int coeffMax = 32767;

	int M = Helpers::logarithm2(N);

	//TODO: Calculate bdShift as follows: bdShift = Max( 20 − bitDepth, extended_precision_processing_flag ? 11 : 0 )
	int bdShift = (bitDepth + M - 9);

	int16_t* D = new int16_t[N * N];

	switch (N) {
	case 32:
		multiply(D32, A, N, D, bdShift);
		break;

	case 16:
		multiply(D16, A, N, D, bdShift);
		break;

	case 8:
		multiply(D8, A, N, D, bdShift);
		break;

	case 4:
		multiply(D4, A, N, D, bdShift);
		break;

	default:
		break;
	}

	bdShift = M + 6;

	switch (N) {
	case 32:
		multiply(D, D32T, N, C, bdShift);
		break;

	case 16:
		multiply(D, D16T, N, C, bdShift);
		break;

	case 8:
		multiply(D, D8T, N, C, bdShift);
		break;

	case 4:
		multiply(D, D4T, N, C, bdShift);
		break;

	default:
		break;
	}

	delete[] D;
}

void FunctionsCPU::substract(volatile unsigned char* rawBlock, volatile unsigned char* predictedBlock, int16_t* residualBlock, int blockSize)
{
	for (int i = 0; i < blockSize * blockSize; i++)
	{
		residualBlock[i] = rawBlock[i] - predictedBlock[i];
	}
}

void FunctionsCPU::rgb2yuv(volatile int16_t* R, volatile int16_t* G, volatile int16_t* B, int blockSize, int16_t* Y, int16_t* U, int16_t* V)
{
	float factors[9] = {0.299, 0.587, 0.114, -0.1687, -0.3313, 0.5, 0.5, -0.4187, -0.0813};

	for (int i = 0; i < blockSize * blockSize; i++)
	{
		Y[i] = 0.299 * R[i] + 0.587 * G[i] + 0.114 * B[i];
		U[i] = -0.1687 * R[i] - 0.3313 * G[i] + 0.5 * B[i] + 128;
		V[i] = 0.5 * R[i] - 0.4187 * G[i] - 0.0813 * B[i] + 128;
	}
}

void FunctionsCPU::rgb2yuvShift(volatile int16_t* R, volatile int16_t* G, volatile int16_t* B, int blockSize, int16_t* Y, int16_t* U, int16_t* V)
{
	for (int i = 0; i < blockSize * blockSize; i++)
	{
		Y[i] = (R[i] >> 2) +  (G[i] >> 1);
		U[i] = - (G[i] >> 2) + (B[i] >> 1) + 128;
		V[i] = (R[i] >> 1) - (G[i] >> 1) + 128;
	}
}