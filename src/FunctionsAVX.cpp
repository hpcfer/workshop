﻿#include "FunctionsAVX.h"


FunctionsAVX::FunctionsAVX()
{
}

int FunctionsAVX::SAD(volatile unsigned char* original, volatile unsigned char* predicted, int puWidth, int puHeight)
{

	__m256i a, b, sad, sadtemp;
	sad = _mm256_set1_epi32(0);


	for (int i = 0; i < puWidth * puHeight; i = i + 32)
	{
		a = _mm256_loadu_si256((__m256i*) & original[i]);
		b = _mm256_loadu_si256((__m256i*) & predicted[i]);

		sadtemp = _mm256_sad_epu8(a, b);
		sad = _mm256_add_epi64(sadtemp, sad);
	}


	int res = ((int*)&sad)[0] + ((int*)&sad)[2] + ((int*)&sad)[4] + ((int*)&sad)[6];

	return res;
}

void FunctionsAVX::quantization(volatile int16_t* A, int QP, int NB, int N, bool* is_zero_matrix, int16_t* C)
{
	int M = Helpers::logarithm2(N);

	int bdShift = 29 - M - NB;

	int coeffMin = -32768;
	int coeffMax = 32767;

	int mScalingFactor = 16;
	int i;
	*is_zero_matrix = true;

	__m256i mult, offset, temp0, temp1, all_1;


	mult = _mm256_set1_epi32(f[QP % 6]);
	offset = _mm256_set1_epi32(1 << (bdShift - 1));
	all_1 = _mm256_set1_epi32(UINT32_MAX);

	for (i = 0; i < N * N; i = i + 16)
	{
		__m256i _a0 = _mm256_loadu_si256((__m256i*) & A[i]);
		__m256i _a1;

		__m128i _a_lo = _mm256_extracti128_si256(_a0, 0);
		__m128i _a_hi = _mm256_extracti128_si256(_a0, 1);

		_a0 = _mm256_cvtepi16_epi32(_a_lo);
		_a1 = _mm256_cvtepi16_epi32(_a_hi);

		__m256i _b0 = _mm256_sign_epi32(_a0, _a0);
		__m256i _b1 = _mm256_sign_epi32(_a1, _a1);


		temp0 = _mm256_mullo_epi32(mult, _b0);
		temp0 = _mm256_add_epi32(offset, temp0);
		temp0 = _mm256_srai_epi32(temp0, (QP / 6) + bdShift);
		temp0 = _mm256_sign_epi32(temp0, _a0);

		temp1 = _mm256_mullo_epi32(mult, _b1);
		temp1 = _mm256_add_epi32(offset, temp1);
		temp1 = _mm256_srai_epi32(temp1, (QP / 6) + bdShift);
		temp1 = _mm256_sign_epi32(temp1, _a1);

		if (_mm256_testz_si256(all_1, temp0) == 0)
			*is_zero_matrix = false;

		if (_mm256_testz_si256(all_1, temp1) == 0)
			*is_zero_matrix = false;

		temp0 = _mm256_packs_epi32(temp0, temp1);
		temp0 = _mm256_permute4x64_epi64(temp0, 0xD8);

		_mm256_storeu_si256((__m256i*) & C[i], temp0);

	}


}

void FunctionsAVX::dequantization(volatile int16_t* A, int QP, int bitDepth, int N, int16_t* C)
{
	int M = Helpers::logarithm2(N);

	int log2TransformRange = 15;
	int bdShift = bitDepth + M + 10 - log2TransformRange;
	int coeffMin = -32768;
	int coeffMax = 32767;
	int mScalingFactor = 16;
	int i;

	__m256i mult, mult2, offset, temp0, temp1;


	mult = _mm256_set1_epi32(mScalingFactor);
	mult2 = _mm256_set1_epi32(g[QP % 6]);
	mult2 = _mm256_slli_epi32(mult2, QP / 6);
	offset = _mm256_set1_epi32(1 << (bdShift - 1));

	for (i = 0; i < N * N; i = i + 16)
	{
		__m256i _a0 = _mm256_loadu_si256((__m256i*) & A[i]);
		__m256i _a1;

		__m128i _a_lo = _mm256_extracti128_si256(_a0, 0);
		__m128i _a_hi = _mm256_extracti128_si256(_a0, 1);

		_a0 = _mm256_cvtepi16_epi32(_a_lo);
		_a1 = _mm256_cvtepi16_epi32(_a_hi);

		temp0 = _mm256_mullo_epi32(mult, _a0);
		temp0 = _mm256_mullo_epi32(mult2, temp0);
		temp0 = _mm256_add_epi32(offset, temp0);
		temp0 = _mm256_srai_epi32(temp0, bdShift);

		temp1 = _mm256_mullo_epi32(mult, _a1);
		temp1 = _mm256_mullo_epi32(mult2, temp1);
		temp1 = _mm256_add_epi32(offset, temp1);
		temp1 = _mm256_srai_epi32(temp1, bdShift);

		temp0 = _mm256_packs_epi32(temp0, temp1);
		temp0 = _mm256_permute4x64_epi64(temp0, 0xD8);

		_mm256_storeu_si256((__m256i*) & C[i], temp0);

	}

}

void FunctionsAVX::multiply(volatile int16_t* A, volatile int16_t* B, int N, int16_t* C, int bdShift)
{
	int shiftOffset = 1 << (bdShift - 1);
	if (N == 4)
	{
		__m256i temp;
		__m256i r_32[4];
		r_32[0] = _mm256_loadu_si256((__m256i*) B);
		r_32[1] = _mm256_permute4x64_epi64(r_32[0], 0xB1);

		// 22 44
		temp = _mm256_blend_epi32(r_32[0], r_32[1], 0x33);

		//22
		__m128i r0_16 = _mm256_extracti128_si256(temp, 0);

		//44
		__m128i r1_16 = _mm256_extracti128_si256(temp, 1);

		// 11 33
		temp = _mm256_blend_epi32(r_32[0], r_32[1], 0xCC);

		r_32[1] = _mm256_cvtepi16_epi32(r0_16);
		r_32[3] = _mm256_cvtepi16_epi32(r1_16);

		// 11
		r0_16 = _mm256_extracti128_si256(temp, 0);

		// 33
		r1_16 = _mm256_extracti128_si256(temp, 1);

		r_32[0] = _mm256_cvtepi16_epi32(r0_16);
		r_32[2] = _mm256_cvtepi16_epi32(r1_16);

		__m256i c[2];
		c[0] = _mm256_set1_epi32(0);
		c[1] = _mm256_set1_epi32(0);

		for (int i = 0; i < N; i = i + 2)
		{
			__m256i mult, mult1;
			__m128i mult0;

			mult0 = _mm_set1_epi32(A[i * N]);
			mult1 = _mm256_set1_epi32(A[(i + 1) * N]);
			mult = _mm256_inserti128_si256(mult1, mult0, 0);

			temp = _mm256_mullo_epi32(mult, r_32[0]);
			c[i / 2] = _mm256_add_epi32(c[i / 2], temp);

			mult0 = _mm_set1_epi32(A[i * N + 1]);
			mult1 = _mm256_set1_epi32(A[(i + 1) * N + 1]);
			mult = _mm256_inserti128_si256(mult1, mult0, 0);

			temp = _mm256_mullo_epi32(mult, r_32[1]);
			c[i / 2] = _mm256_add_epi32(c[i / 2], temp);

			mult0 = _mm_set1_epi32(A[i * N + 2]);
			mult1 = _mm256_set1_epi32(A[((i + 1) * N + 2)]);
			mult = _mm256_inserti128_si256(mult1, mult0, 0);

			temp = _mm256_mullo_epi32(mult, r_32[2]);
			c[i / 2] = _mm256_add_epi32(c[i / 2], temp);

			mult0 = _mm_set1_epi32(A[i * N + 3]);
			mult1 = _mm256_set1_epi32(A[((i + 1) * N + 3)]);
			mult = _mm256_inserti128_si256(mult1, mult0, 0);

			temp = _mm256_mullo_epi32(mult, r_32[3]);
			c[i / 2] = _mm256_add_epi32(c[i / 2], temp);
		}

		temp = _mm256_set1_epi32(shiftOffset);
		c[0] = _mm256_add_epi32(c[0], temp);
		c[0] = _mm256_srai_epi32(c[0], bdShift);

		c[1] = _mm256_add_epi32(c[1], temp);
		c[1] = _mm256_srai_epi32(c[1], bdShift);

		c[0] = _mm256_packs_epi32(c[0], c[1]);
		c[0] = _mm256_permute4x64_epi64(c[0], 0xD8);
		_mm256_storeu_si256((__m256i*) C, c[0]);

	}

	if (N == 8)
	{
		//first 2 rows 16 bit.
		__m256i r_32[8];

		//load B matrix into registers
		for (int i = 0; i < N; i = i + 2)
		{
			r_32[i] = _mm256_loadu_si256((__m256i*) & B[i * 8]);
			__m128i r0 = _mm256_extracti128_si256(r_32[i], 0);
			__m128i r1 = _mm256_extracti128_si256(r_32[i], 1);
			r_32[i] = _mm256_cvtepi16_epi32(r0);
			r_32[i + 1] = _mm256_cvtepi16_epi32(r1);
		}


		for (int i = 0; i < N; i = i + 2)
		{
			__m256i temp;
			__m256i c0 = _mm256_set1_epi32(0);
			__m256i c1 = _mm256_set1_epi32(0);

			for (int j = 0; j < N; j++)
			{

				__m256i mult;



				mult = _mm256_set1_epi32(A[i * N + j]);
				temp = _mm256_mullo_epi32(mult, r_32[j]);
				c0 = _mm256_add_epi32(c0, temp);

				mult = _mm256_set1_epi32(A[(i + 1) * N + j]);
				temp = _mm256_mullo_epi32(mult, r_32[j]);
				c1 = _mm256_add_epi32(c1, temp);
			}

			temp = _mm256_set1_epi32(shiftOffset);
			c0 = _mm256_add_epi32(c0, temp);
			c0 = _mm256_srai_epi32(c0, bdShift);

			c1 = _mm256_add_epi32(c1, temp);
			c1 = _mm256_srai_epi32(c1, bdShift);

			c0 = _mm256_packs_epi32(c0, c1);
			c0 = _mm256_permute4x64_epi64(c0, 0xD8);

			_mm256_storeu_si256((__m256i*) & C[i * 8], c0);
		}
	}

	else if (N == 16)
	{

		for (int i = 0; i < N; i++)
		{
			__m256i c0 = _mm256_set1_epi32(0);
			__m256i c1 = _mm256_set1_epi32(0);

			__m256i mult;
			__m256i temp0, temp1;


			for (int j = 0; j < N; j++)
			{

				mult = _mm256_set1_epi32(A[i * 16 + j]);

				__m256i _b0 = _mm256_loadu_si256((__m256i*) & B[j * 16]);
				__m256i _b1;

				//lower 128 bits
				__m128i lo = _mm256_extracti128_si256(_b0, 0);

				//higher 128 bits
				__m128i hi = _mm256_extracti128_si256(_b0, 1);

				_b0 = _mm256_cvtepi16_epi32(lo);
				temp0 = _mm256_mullo_epi32(mult, _b0);
				c0 = _mm256_add_epi32(c0, temp0);

				_b1 = _mm256_cvtepi16_epi32(hi);
				temp1 = _mm256_mullo_epi32(mult, _b1);
				c1 = _mm256_add_epi32(c1, temp1);
			}

			temp0 = _mm256_set1_epi32(shiftOffset);
			temp1 = _mm256_set1_epi32(shiftOffset);
			c0 = _mm256_add_epi32(c0, temp0);
			c0 = _mm256_srai_epi32(c0, bdShift);

			c1 = _mm256_add_epi32(c1, temp1);
			c1 = _mm256_srai_epi32(c1, bdShift);

			c0 = _mm256_packs_epi32(c0, c1);

			c0 = _mm256_permute4x64_epi64(c0, 0xD8);

			_mm256_storeu_si256((__m256i*) & C[i * 16], c0);

		}

	}

	else if (N == 32)
	{

		for (int i = 0; i < N; i++)
		{
			__m256i c0 = _mm256_set1_epi32(0);
			__m256i c1 = _mm256_set1_epi32(0);
			__m256i c2 = _mm256_set1_epi32(0);
			__m256i c3 = _mm256_set1_epi32(0);

			__m256i mult;
			__m256i temp0, temp1, temp2, temp3;

			for (int j = 0; j < N; j++)
			{
				__m256i _b0 = _mm256_loadu_si256((__m256i*) & B[j * 32]);
				__m256i _b1;
				__m256i _b2 = _mm256_loadu_si256((__m256i*) & B[j * 32 + 16]);
				__m256i _b3;
				//lower 128 bits
				__m128i lo = _mm256_extracti128_si256(_b0, 0);

				//higher 128 bits
				__m128i hi = _mm256_extracti128_si256(_b0, 1);

				_b0 = _mm256_cvtepi16_epi32(lo);
				_b1 = _mm256_cvtepi16_epi32(hi);

				lo = _mm256_extracti128_si256(_b2, 0);
				hi = _mm256_extracti128_si256(_b2, 1);

				_b2 = _mm256_cvtepi16_epi32(lo);
				_b3 = _mm256_cvtepi16_epi32(hi);

				mult = _mm256_set1_epi32(A[i * 32 + j]);

				temp0 = _mm256_mullo_epi32(mult, _b0);
				temp1 = _mm256_mullo_epi32(mult, _b1);
				temp2 = _mm256_mullo_epi32(mult, _b2);
				temp3 = _mm256_mullo_epi32(mult, _b3);

				c0 = _mm256_add_epi32(c0, temp0);
				c1 = _mm256_add_epi32(c1, temp1);
				c2 = _mm256_add_epi32(c2, temp2);
				c3 = _mm256_add_epi32(c3, temp3);
			}

			temp0 = _mm256_set1_epi32(shiftOffset);
			temp1 = _mm256_set1_epi32(shiftOffset);
			temp2 = _mm256_set1_epi32(shiftOffset);
			temp3 = _mm256_set1_epi32(shiftOffset);

			c0 = _mm256_add_epi32(c0, temp0);
			c0 = _mm256_srai_epi32(c0, bdShift);

			c1 = _mm256_add_epi32(c1, temp1);
			c1 = _mm256_srai_epi32(c1, bdShift);

			c2 = _mm256_add_epi32(c2, temp2);
			c2 = _mm256_srai_epi32(c2, bdShift);

			c3 = _mm256_add_epi32(c3, temp3);
			c3 = _mm256_srai_epi32(c3, bdShift);

			c0 = _mm256_packs_epi32(c0, c1);
			c2 = _mm256_packs_epi32(c2, c3);

			c0 = _mm256_permute4x64_epi64(c0, 0xD8);
			c2 = _mm256_permute4x64_epi64(c2, 0xD8);

			_mm256_storeu_si256((__m256i*) & C[i * 32], c0);
			_mm256_storeu_si256((__m256i*) & C[i * 32 + 16], c2);
		}

	}
}

void FunctionsAVX::dct(volatile int16_t* A, volatile int16_t* B, int N, int16_t* C)
{
	int log2TransformRange = 15;
	int bitDepth = 8;

	int coeffMin = -32768;
	int coeffMax = 32767;

	int M = Helpers::logarithm2(N);

	//TODO: Calculate bdShift as follows: bdShift = Max( 20 − bitDepth, extended_precision_processing_flag ? 11 : 0 )
	int bdShift = (bitDepth + M - 9);

	int16_t* D = new int16_t[N * N];

	switch (N) {
	case 32:
		multiply(D32, A, N, D, bdShift);
		break;

	case 16:
		multiply(D16, A, N, D, bdShift);
		break;

	case 8:
		multiply(D8, A, N, D, bdShift);
		break;

	case 4:
		multiply(D4, A, N, D, bdShift);
		break;

	default:
		break;
	}

	bdShift = M + 6;

	switch (N) {
	case 32:
		multiply(D, D32T, N, C, bdShift);
		break;

	case 16:
		multiply(D, D16T, N, C, bdShift);
		break;

	case 8:
		multiply(D, D8T, N, C, bdShift);
		break;

	case 4:
		multiply(D, D4T, N, C, bdShift);
		break;

	default:
		break;
	}

	delete[] D;
}

void FunctionsAVX::substract(volatile unsigned char* rawBlock, volatile unsigned char* predictedBlock, int16_t* residualBlock, int blockSize)
{

	__m256i a, b, c, a_low, a_high, b_low, b_high, c_low, c_high;
	for (int i = 0; i < blockSize * blockSize; i = i + 32)
	{
		a = _mm256_loadu_si256((__m256i*) & rawBlock[i]);
		b = _mm256_loadu_si256((__m256i*) & predictedBlock[i]);

		a_low = _mm256_cvtepu8_epi16(_mm256_extracti128_si256(a, 0));
		a_high = _mm256_cvtepu8_epi16(_mm256_extracti128_si256(a, 1));

		b_low = _mm256_cvtepu8_epi16(_mm256_extracti128_si256(b, 0));
		b_high = _mm256_cvtepu8_epi16(_mm256_extracti128_si256(b, 1));

		c_low = _mm256_subs_epi16(a_low, b_low);
		c_high = _mm256_subs_epi16(a_high, b_high);

		_mm256_storeu_si256((__m256i*) & residualBlock[i], c_low);
		_mm256_storeu_si256((__m256i*) & residualBlock[i + 16], c_high);
	}
}

void FunctionsAVX::rgb2yuv(volatile int16_t* R, volatile int16_t* G, volatile  int16_t* B, int blockSize, int16_t* Y, int16_t* U, int16_t* V)
{
	//TODO implement rgb2yuv method using AVX/AVX2 intrinsics
}

void FunctionsAVX::rgb2yuvShift(volatile int16_t* R, volatile  int16_t* G, volatile  int16_t* B, int blockSize, int16_t* Y, int16_t* U, int16_t* V)
{
	//TODO implement rgb2yuvShift method using AVX/AVX2 intrinsics
}