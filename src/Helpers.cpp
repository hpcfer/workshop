#include "Helpers.h"

int Helpers::clip3(const int a, const int b, int x)
{
	if (x < a)
		return a;
	else if (x > b)
		return b;
	else
		return x;
}
int Helpers::sign(const int a)
{
	if (a < 0)
		return -1;
	if (a > 0)
		return 1;

	return 0;
}

int Helpers::logarithm2(int a)
{
	if (a == 2)
		return 1;
	else if (a == 4)
		return 2;
	else if (a == 8)
		return 3;
	else if (a == 16)
		return  4;
	else if (a == 32)
		return  5;
	else if (a == 64)
		return  6;
	else if (a == 128)
		return  7;
	else if (a == 256)
		return  8;
	else if (a == 512)
		return  9;
	else if (a == 1024)
		return  10;
	else if (a == 2048)
		return  11;
	else if (a == 4096)
		return  12;
	else if (a == 8192)
		return  13;
	else if (a == 16384)
		return  14;
	else if (a == 32768)
		return  15;
	else if (a == 65536)
		return  16;
	else if (a == 131072)
		return  17;
	else if (a == 262144)
		return  18;
	else if (a == 524288)
		return  19;
	else if (a == 1048576)
		return  20;
	else if (a == 2097152)
		return  21;
	else if (a == 4194304)
		return  22;
	else if (a == 8388608)
		return  23;
	else if (a == 16777216)
		return  24;
	else if (a == 33554432)
		return  25;
	else if (a == 67108864)
		return  26;
	else if (a == 134217728)
		return  27;
	else if (a == 268435456)
		return  28;
	else if (a == 536870912)
		return  29;
	else if (a == 1073741824)
		return  30;
	else if (a == 2147483648)
		return  31;
	else
		return -1;

}

