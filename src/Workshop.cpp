// Workshop.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#pragma once
#include <iostream>
#include "Helpers.h"
#include "FunctionsCPU.h"
#include "FunctionsAVX.h"

using namespace std;
void benchmarkSAD(int blockSize, int iterations);
void benchmarkMultiply(int blockSize, int iterations);
void benchmarkQuantization(int blockSize, int iterations);
void benchmarkSubstract(int blockSize, int iterations);
void benchmarkDCT(int blockSize, int iterations);
void benchmarkRgb2yuv(int blockSize, int iterations);
void benchmarkRgb2yuvShift(int blockSize, int iterations);


int main()
{
	int block_size = 32;
	int iterations = 1000000;

	benchmarkSAD(block_size, iterations);
	benchmarkMultiply(block_size, iterations);
	benchmarkDCT(block_size, iterations);
	benchmarkQuantization(block_size, iterations);
	benchmarkSubstract(block_size, iterations);
	benchmarkRgb2yuv(block_size, iterations);
	benchmarkRgb2yuvShift(block_size, iterations);

}

void benchmarkSAD(int blockSize, int iterations)
{
	cout << endl << "Benchmarking SAD function..." << endl;
	volatile unsigned char* blockA = new volatile unsigned char[blockSize * blockSize];
	volatile unsigned char* blockB = new volatile unsigned char[blockSize * blockSize];

	srand(time(NULL));
	Helpers::generateRandomResidual(blockA, blockSize, 255);
	Helpers::generateRandomResidual(blockB, blockSize, 255);

	volatile int sad = 0;
	volatile int sad_avx = 0;

	clock_t begin_regular = clock();
	for (int i = 0; i < iterations; i++)
	{
		sad = FunctionsCPU::SAD(blockA, blockB, blockSize, blockSize);
	}
	float end_regular = float(clock() - begin_regular);
	cout << setprecision(5) << " Processing time (regular): " << end_regular / CLOCKS_PER_SEC << " s " << endl;

	clock_t begin_avx = clock();
	for (int i = 0; i < iterations; i++)
	{
		sad_avx = FunctionsAVX::SAD(blockA, blockB, blockSize, blockSize);
	}

	float end_avx = float(clock() - begin_avx);
	cout << setprecision(5) << " Processing time (avx): " << end_avx / CLOCKS_PER_SEC << " s " << endl;

	delete[] blockA;
	delete[] blockB;
	cout << "Benchmarking SAD function finished!" << endl;
}

void benchmarkMultiply(int blockSize, int iterations)
{
	cout << endl << "Benchmarking multiply function..." << endl;

	volatile int16_t* inputA = new volatile int16_t[blockSize * blockSize];
	volatile int16_t* inputB = new volatile int16_t[blockSize * blockSize];
	int16_t* output_cpu = new int16_t[blockSize * blockSize];
	int16_t* output_avx = new int16_t[blockSize * blockSize];

	int M = Helpers::logarithm2(blockSize);
	int bits = 8;
	int bdShift = bits + M - 9;

	srand(time(NULL));
	Helpers::generateRandomResidual(inputA, blockSize, 32767);
	Helpers::generateRandomResidual(inputB, blockSize, 32767);

	clock_t begin_regular = clock();
	for (int i = 0; i < iterations; i++)
	{
		FunctionsCPU::multiply(inputA, inputB, blockSize, output_cpu, bdShift);
	}
	float end_regular = float(clock() - begin_regular);
	cout << setprecision(5) << " Processing time (regular): " << end_regular / CLOCKS_PER_SEC << " s " << endl;

	clock_t begin_avx = clock();
	for (int i = 0; i < iterations; i++)
	{
		FunctionsAVX::multiply(inputA, inputB, blockSize, output_avx, bdShift);
	}

	float end_avx = float(clock() - begin_avx);
	cout << setprecision(5) << " Processing time (avx): " << end_avx / CLOCKS_PER_SEC << " s " << endl;

	cout << "Benchmarking multiply function finished!" << endl;

	delete[] inputA;
	delete[] inputB;
	delete[] output_avx;
	delete[] output_cpu;
}

void benchmarkDCT(int blockSize, int iterations)
{
	cout << endl << "Benchmarking DCT function..." << endl;

	volatile int16_t* inputA = new volatile int16_t[blockSize * blockSize];
	volatile int16_t* inputB = new volatile int16_t[blockSize * blockSize];
	int16_t* output_cpu = new int16_t[blockSize * blockSize];
	int16_t* output_avx = new int16_t[blockSize * blockSize];

	srand(time(NULL));
	Helpers::generateRandomResidual(inputA, blockSize, 32767);
	Helpers::generateRandomResidual(inputB, blockSize, 32767);

	clock_t begin_regular = clock();
	for (int i = 0; i < iterations; i++)
	{
		FunctionsCPU::dct(inputA, inputB, blockSize, output_cpu);
	}
	float end_regular = float(clock() - begin_regular);
	cout << setprecision(5) << " Processing time (regular): " << end_regular / CLOCKS_PER_SEC << " s " << endl;

	clock_t begin_avx = clock();
	for (int i = 0; i < iterations; i++)
	{
		FunctionsAVX::dct(inputA, inputB, blockSize, output_avx);
	}

	float end_avx = float(clock() - begin_avx);
	cout << setprecision(5) << " Processing time (avx): " << end_avx / CLOCKS_PER_SEC << " s " << endl;

	cout << "Benchmarking DCT function finished!" << endl;

	delete[] inputA;
	delete[] inputB;
	delete[] output_avx;
	delete[] output_cpu;
}

void benchmarkQuantization(int blockSize, int iterations)
{
	cout << endl << "Benchmarking quantization function..." << endl;

	volatile int16_t* inputA = new volatile int16_t[blockSize * blockSize];
	int16_t* output_cpu = new int16_t[blockSize * blockSize];
	int16_t* output_avx = new int16_t[blockSize * blockSize];

	int bits = 8;
	int qp = 22;
	bool isZero = false;

	srand(time(NULL));
	Helpers::generateRandomResidual(inputA, blockSize, 32767);

	clock_t begin_regular = clock();
	for (int i = 0; i < iterations; i++)
	{
		FunctionsCPU::quantization(inputA, qp, bits, blockSize, &isZero, output_cpu);
	}
	float end_regular = float(clock() - begin_regular);
	cout << setprecision(5) << " Processing time (regular): " << end_regular / CLOCKS_PER_SEC << " s " << endl;

	clock_t begin_avx = clock();
	for (int i = 0; i < iterations; i++)
	{
		FunctionsAVX::quantization(inputA, qp, bits, blockSize, &isZero, output_avx);
	}

	float end_avx = float(clock() - begin_avx);
	cout << setprecision(5) << " Processing time (avx): " << end_avx / CLOCKS_PER_SEC << " s " << endl;

	cout << "Benchmarking quantization function finished!" << endl;

	delete[] inputA;
	delete[] output_avx;
	delete[] output_cpu;
}

void benchmarkSubstract(int blockSize, int iterations)
{
	cout << endl << "Benchmarking substract function..." << endl;

	//blockSize = 32;

	volatile unsigned char* inputA = new volatile unsigned char[blockSize * blockSize] {0};
	volatile unsigned char* inputB = new volatile  unsigned char[blockSize * blockSize] {0};
	int16_t* output_cpu = new int16_t[blockSize * blockSize]{ 0 };
	int16_t* output_avx = new int16_t[blockSize * blockSize]{ 0 };

	srand(time(NULL));
	Helpers::generateRandomResidual(inputA, blockSize, 255);
	Helpers::generateRandomResidual(inputB, blockSize, 255);

	clock_t begin_regular = clock();
	for (int i = 0; i < iterations; i++)
	{
		FunctionsCPU::substract(inputA, inputB, output_cpu, blockSize);
	}
	float end_regular = float(clock() - begin_regular);
	cout << setprecision(5) << " Processing time (regular): " << end_regular / CLOCKS_PER_SEC << " s " << endl;

	clock_t begin_avx = clock();
	for (int i = 0; i < iterations; i++)
	{
		FunctionsAVX::substract(inputA, inputB, output_avx, blockSize);
	}

	float end_avx = float(clock() - begin_avx);
	cout << setprecision(5) << " Processing time (avx): " << end_avx / CLOCKS_PER_SEC << " s " << endl;

	cout << "Benchmarking substract function finished!" << endl;

	delete[] inputA;
	delete[] inputB;
	delete[] output_avx;
	delete[] output_cpu;
}

void benchmarkRgb2yuv(int blockSize, int iterations)
{
	cout << endl << "Benchmarking rgb2yuv function..." << endl;

	volatile int16_t* inputA = new volatile int16_t[blockSize * blockSize];
	volatile int16_t* inputB = new volatile int16_t[blockSize * blockSize];
	volatile int16_t* inputC = new volatile int16_t[blockSize * blockSize];

	int16_t* Y_cpu = new int16_t[blockSize * blockSize];
	int16_t* U_cpu = new int16_t[blockSize * blockSize];
	int16_t* V_cpu = new int16_t[blockSize * blockSize];

	int16_t* Y_avx = new int16_t[blockSize * blockSize];
	int16_t* U_avx = new int16_t[blockSize * blockSize];
	int16_t* V_avx = new int16_t[blockSize * blockSize];

	srand(time(NULL));
	Helpers::generateRandomPositiveResidual(inputA, blockSize, 255);
	Helpers::generateRandomPositiveResidual(inputB, blockSize, 255);
	Helpers::generateRandomPositiveResidual(inputC, blockSize, 255);

	clock_t begin_regular = clock();
	for (int i = 0; i < iterations; i++)
	{
		FunctionsCPU::rgb2yuv(inputA, inputB, inputC, blockSize, Y_cpu, U_cpu, V_cpu);
	}
	float end_regular = float(clock() - begin_regular);
	cout << setprecision(5) << " Processing time (regular): " << end_regular / CLOCKS_PER_SEC << " s " << endl;

	clock_t begin_avx = clock();
	for (int i = 0; i < iterations; i++)
	{
		FunctionsAVX::rgb2yuv(inputA, inputB, inputC, blockSize, Y_avx, U_avx, V_avx);
	}

	float end_avx = float(clock() - begin_avx);
	cout << setprecision(5) << " Processing time (avx): " << end_avx / CLOCKS_PER_SEC << " s " << endl;

	cout << "Benchmarking rgb2yuv function finished!" << endl;

	delete[] inputA;
	delete[] inputB;
	delete[] inputC;

	delete[] Y_cpu;
	delete[] U_cpu;
	delete[] V_cpu;

	delete[] Y_avx;
	delete[] U_avx;
	delete[] V_avx;
}

void benchmarkRgb2yuvShift(int blockSize, int iterations)
{
	cout << endl << "Benchmarking rgb2yuvShift function..." << endl;

	volatile int16_t* inputA = new volatile int16_t[blockSize * blockSize];
	volatile int16_t* inputB = new volatile int16_t[blockSize * blockSize];
	volatile int16_t* inputC = new volatile int16_t[blockSize * blockSize];

	int16_t* Y_cpu = new int16_t[blockSize * blockSize];
	int16_t* U_cpu = new int16_t[blockSize * blockSize];
	int16_t* V_cpu = new int16_t[blockSize * blockSize];

	int16_t* Y_avx = new int16_t[blockSize * blockSize];
	int16_t* U_avx = new int16_t[blockSize * blockSize];
	int16_t* V_avx = new int16_t[blockSize * blockSize];

	srand(time(NULL));
	Helpers::generateRandomPositiveResidual(inputA, blockSize, 255);
	Helpers::generateRandomPositiveResidual(inputB, blockSize, 255);
	Helpers::generateRandomPositiveResidual(inputC, blockSize, 255);

	clock_t begin_regular = clock();
	for (int i = 0; i < iterations; i++)
	{
		FunctionsCPU::rgb2yuvShift(inputA, inputB, inputC, blockSize, Y_cpu, U_cpu, V_cpu);
	}

	float end_regular = float(clock() - begin_regular);
	cout << setprecision(5) << " Processing time (regular): " << end_regular / CLOCKS_PER_SEC << " s " << endl;

	clock_t begin_avx = clock();
	for (int i = 0; i < iterations; i++)
	{
		FunctionsAVX::rgb2yuvShift(inputA, inputB, inputC, blockSize, Y_avx, U_avx, V_avx);
	}

	float end_avx = float(clock() - begin_avx);
	cout << setprecision(5) << " Processing time (avx): " << end_avx / CLOCKS_PER_SEC << " s " << endl;

	cout << "Benchmarking rgb2yuvShift function finished!" << endl;

	delete[] inputA;
	delete[] inputB;
	delete[] inputC;

	delete[] Y_cpu;
	delete[] U_cpu;
	delete[] V_cpu;

	delete[] Y_avx;
	delete[] U_avx;
	delete[] V_avx;
}